from .models import Trash, RecoveryTask, RemoveTask,\
    ClearTask, PathsByRemoveTask, NamesForRecoveryTask
from smartRMV.rm.control import create_folder
from os import path
import shutil
from django.utils import timezone


def create_new_remove_task(trash, data):
    task = RemoveTask.objects.create(mode=data['mode'], trash=trash, force=data['force'],
                                     reg_expression=data['reg_expression'], creating_date=timezone.datetime.now())

    task.save()
    for pth in data['paths'].split('\n'):
        PathsByRemoveTask.objects.create(task=task, path=pth.replace('\r', ''))


def create_new_clear_task(trash, data):
    task = ClearTask.objects.create(policy=data['policy'], trash=trash, max_amount=data["max_amount"],
                                    days=data["days"], force=data['force'], creating_date=timezone.datetime.now())
    task.save()


def create_new_rec_task(trash, data):
    task = RecoveryTask.objects.create(trash=trash, force=data['force'], creating_date=timezone.datetime.now())
    task.save()
    for name in data['names'].split('\n'):
        NamesForRecoveryTask.objects.create(task=task, name=name.replace('\r', ''))


def delete_clear_task(trash):
    clear_tasks_set = trash.cleartask_set.all()
    if len(clear_tasks_set) > 0:
        clear_tasks_set[0].delete()


def delete_remove_task(trash):
    remove_tasks_set = trash.removetask_set.all()
    if len(remove_tasks_set) > 0:
        remove_tasks_set[0].delete()


def delete_recovery_task(trash):
    recovery_tasks_set = trash.recoverytask_set.all()
    if len(recovery_tasks_set) > 0:
        recovery_tasks_set[0].delete()


def create_new_trash(name, pth, max_trash_size):
    pth = path.abspath(pth)
    try:
        if path.exists(pth):
            create_folder(path=path.join(pth, name))
            info_path = path.join(pth, name+"INFO")
            create_folder(info_path)
            with open(name=path.join(info_path, 'content.json'), mode='w') as f:
                f.write('{}')
        else:
            return False
    except OSError:
        return False
    else:
        trash = Trash.objects.create(name=name, path=pth, size=0, max_trash_size=max_trash_size,
                                     last_clearing_date=timezone.datetime.now().date(), files_count=0)
        trash.save()
        return True


def delete_trash(pk):
    trash = Trash.objects.get(id=pk)
    trash.delete()
    try:
        shutil.rmtree(trash.get_trash_path())
        shutil.rmtree(trash.get_trash_path()+"INFO")
    except OSError as er:
        print er.message


def get_trash_context(trash):
    clear_tasks = trash.cleartask_set.all()
    remove_tasks = trash.removetask_set.all()
    recovery_tasks = trash.recoverytask_set.all()
    context = {'trash': trash, 'clear_tasks': clear_tasks, 'remove_tasks': remove_tasks,
               'recovery_tasks': recovery_tasks, }
    return context
