from django import forms
from models import ClearTask, RemoveTask, RecoveryTask, Trash, models
import os


class TrashForm(forms.ModelForm):
    class Meta:
        model = Trash
        fields = ['path', 'name', 'max_trash_size']
        labels = {'path': 'Trash path', 'name': 'Trash name', 'max_trash_size': 'Max trash size'}

    def clean_path(self):
        path = self.cleaned_data['path']
        if not os.path.exists(path):
            raise forms.ValidationError("Not found path!")
        return path

    def clean_max_trash_size(self):
        if self.cleaned_data['max_trash_size'] <= 0:
            raise forms.ValidationError("Trash size can not be less than zero!")
        else:
            return self.cleaned_data['max_trash_size']

    def clean(self):
        super(TrashForm, self).clean()
        path = self.cleaned_data['path']
        name = self.cleaned_data['name']
        if os.path.exists(os.path.join(path, name)):
            raise forms.ValidationError("Folder by this path exists! Write new folder name!")


class RecoveryTaskForm(forms.ModelForm):
    names = forms.CharField(widget=forms.Textarea, max_length=200, label='Names for recovering',
                            help_text="Enter names for recovering. Each name from new line")

    class Meta:
        model = RecoveryTask
        fields = ['force']
        labels = {'force': 'Force mode:'}

    """def clean(self):
        super(RecoveryTaskForm, self).clean()
        file_or_dir_name = self.cleaned_data['file_or_dir_name']
        trash = self.cleaned_data['trash']
        if not trash.removedobject_set.all().filter(cur_name=file_or_dir_name):
            raise forms.ValidationError("Not found file or directory with this name!")"""


class ClearTaskForm(forms.ModelForm):
    class Meta:
        model = ClearTask
        fields = ['policy', 'force', 'max_amount', 'days']
        labels = {'policy': 'Clear policy:', 'force': 'Force mode:',
                  'max_amount': 'Size for file or directory', 'days': 'Count of days'}

    def clean_max_amount(self):
        if self.cleaned_data['max_amount'] <= 0:
            raise forms.ValidationError("Max file or directory size can not be less than zero!")
        else:
            return self.cleaned_data['max_amount']

    def clean_days(self):
        if self.cleaned_data['days'] <= 0:
            raise forms.ValidationError("Days count can not be less than zero!")
        else:
            return self.cleaned_data['days']


class RemoveTaskForm(forms.ModelForm):
    paths = forms.CharField(widget=forms.Textarea, max_length=300, label='Paths of removing',
                            help_text="Enter paths for removing. Each path from new line")

    class Meta:
        model = RemoveTask
        fields = ['mode', 'force', 'reg_expression']
        labels = {'force': 'Force mode:', 'mode': 'Mode of removing'}

    def clean_paths(self):
        paths = self.cleaned_data['paths']
        for path in paths.split('\n'):
            abs_path = os.path.abspath(path.replace('\r', ''))
            if not os.path.exists(abs_path):
                raise forms.ValidationError("Not found file or directory with this path : {path}".format(path=path))
        return paths
