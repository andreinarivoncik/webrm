# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Trash, ClearTask, RecoveryTask, RemoveTask, TaskStatisticObject, PathsByRemoveTask

admin.site.register(Trash)
admin.site.register(ClearTask)
admin.site.register(RemoveTask)
admin.site.register(RecoveryTask)
admin.site.register(TaskStatisticObject)
admin.site.register(PathsByRemoveTask)
