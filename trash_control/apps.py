# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TrashcontrolConfig(AppConfig):
    name = 'trash_control'
    default_trash_path = '/home/andrei/trashes'
    max_trash_size = 1000000
    days = 10
    max_amount = 10000
