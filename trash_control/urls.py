from django.conf.urls import url
from . import views


app_name = 'trash_control'

urlpatterns = [
    url(r'^$', view=views.home_page, name='home_page'),
    url(r'^trashes/$', views.trashes_view, name='trashes_view'),
    url(r'^trashes/administration/$', views.trash_administration, name='trash_administration'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/$', views.trash_detail, name='trash_detail'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/trash_statistic$', views.trash_statistic, name='trash_statistic'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/create_recovery_task/$', views.create_recovery_task,
        name='create_recovery_task'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/create_clear_task/$', views.create_clear_task,
        name='create_clear_task'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/create_remove_task/$', views.create_remove_task,
        name='create_remove_task'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/clear_task_detail/(?P<clear_task_id>[0-9]+)/$',
        views.clear_task_detail, name='clear_task_detail'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/remove_task_detail/(?P<remove_task_id>[0-9]+)/$',
        views.remove_task_detail, name='remove_task_detail'),
    url(r'^trashes/(?P<trash_id>[0-9]+)/recovery_task_detail/(?P<recovery_task_id>[0-9]+)/$',
        views.recovery_task_detail, name='recovery_task_detail')
]
