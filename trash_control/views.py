# -*- coding: utf-8 -*-
import datetime
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .with_models_work import *
from .models import Trash, ClearTask, RemoveTask, RecoveryTask
from forms import TrashForm, RecoveryTaskForm, ClearTaskForm, RemoveTaskForm


def clear_task_detail(request, trash_id, clear_task_id):
    clear_task = get_object_or_404(ClearTask, pk=clear_task_id)
    trash = get_object_or_404(Trash, pk=trash_id)
    context = get_trash_context(trash)
    context['clear_task'] = clear_task
    return render(request, 'trash_control/clear_task_detail.html', context)


def remove_task_detail(request, trash_id, remove_task_id):
    remove_task = get_object_or_404(RemoveTask, pk=remove_task_id)
    trash = get_object_or_404(Trash, pk=trash_id)
    context = get_trash_context(trash)
    context['remove_task'] = remove_task
    return render(request, 'trash_control/remove_task_detail.html', context)


def recovery_task_detail(request, trash_id, recovery_task_id):
    recovery_task = get_object_or_404(RecoveryTask, pk=recovery_task_id)
    trash = get_object_or_404(Trash, pk=trash_id)
    context = get_trash_context(trash)
    context['recovery_task'] = recovery_task
    return render(request, 'trash_control/recovery_task_detail.html', context)


def create_remove_task(request, trash_id):
    if request.method == "GET":
        if request.GET.get("create_remove_task"):
            remove_task_form = RemoveTaskForm()
            return render(request, 'trash_control/create_remove_task.html', {'form': remove_task_form,
                                                                             'trash_id': trash_id})
    if request.method == "POST":
        if request.POST.get("create_remove_task"):
            remove_task_form = RemoveTaskForm(request.POST)
            result = ""
            if remove_task_form.is_valid():
                trash = Trash.objects.get(id=trash_id)
                data = remove_task_form.cleaned_data
                create_new_remove_task(trash, data)
                remove_task_form = RemoveTaskForm()
                result = 'Task was successfully created'
            return render(request, 'trash_control/create_remove_task.html',
                          {'form': remove_task_form, 'trash_id': trash_id, 'result': result})


def create_clear_task(request, trash_id):
    if request.method == "GET":
        if request.GET.get("create_clear_task"):
            clear_task_form = ClearTaskForm()
            return render(request, 'trash_control/create_clear_task.html', {'form': clear_task_form,
                                                                            'trash_id': trash_id})
    if request.method == "POST":
        if request.POST.get("create_clear_task"):
            clear_task_form = ClearTaskForm(request.POST)
            result = ""
            if clear_task_form.is_valid():
                trash = get_object_or_404(Trash, pk=trash_id)
                data = clear_task_form.cleaned_data
                create_new_clear_task(trash, data)
                clear_task_form = ClearTaskForm()
                result = 'Task was successfully created'
            return render(request, 'trash_control/create_clear_task.html',
                          {'form': clear_task_form, 'trash_id': trash_id, 'result': result})


def create_recovery_task(request, trash_id):
    if request.method == "GET":
        if request.GET.get("create_recovery_task"):
            recovery_task_form = RecoveryTaskForm()
            return render(request, 'trash_control/create_recovery_task.html', {'form': recovery_task_form,
                                                                               'trash_id': trash_id})
    if request.method == "POST":
        if request.POST.get("create_recovery_task"):
            recovery_task_form = RecoveryTaskForm(request.POST)
            result = ""
            if recovery_task_form.is_valid():
                trash = get_object_or_404(Trash, pk=trash_id)
                data = recovery_task_form.cleaned_data
                create_new_rec_task(trash, data)
                recovery_task_form = RecoveryTaskForm()
                result = 'Task was successfully created'
            return render(request, 'trash_control/create_recovery_task.html',
                          {'form': recovery_task_form, 'trash_id': trash_id, 'result': result})


def trash_administration(request):
    trashes_list = Trash.objects.all()
    context = {
        'trashes_list': trashes_list,
    }
    if request.method == "GET":
        if request.GET.get("del"):
            context["title"] = "Trash delete panel."
            return render(request, 'trash_control/trash_delete.html', context)
        elif request.GET.get("create"):
            trash_form = TrashForm
            context['trash_form'] = trash_form
            context['title'] = "Trash create panel."
            return render(request, 'trash_control/trash_create.html', context)

    if request.method == "POST":
        if request.POST.get("deleted_trash"):
            delete_trash(request.POST["deleted_trash"])
        elif request.POST.get("created_trash"):
            trash_form = TrashForm(request.POST)
            context['trash_form'] = trash_form
            if trash_form.is_valid():
                data = trash_form.cleaned_data
                create_new_trash(data["name"], data["path"], data["max_trash_size"])
            else:
                context["title"] = "Error by trash creating. Data is not valid. Try again!"
                return render(request, 'trash_control/trash_create.html', context)
    return HttpResponseRedirect(reverse('trash_control:trashes_view'))
    # revers - access by URL by name. Can be set other args or kwargs


def trashes_view(request):
    trashes_list = Trash.objects.all()
    context = {
        'trashes_list': trashes_list,
    }
    return render(request, 'trash_control/trashes_view.html', context)


def trash_statistic(request, trash_id):
    if request.GET.get('trash_statistic'):
        trash = Trash.objects.get(id=trash_id)
        statistics = trash.taskstatisticobject_set.all()
        context = {'statistics': statistics, 'trash': trash}
        return render(request, 'trash_control/trash_statistic.html', context)


def trash_detail(request, trash_id):
    trash = get_object_or_404(Trash, pk=trash_id)
    context = get_trash_context(trash)
    if request.method == 'POST':
        _handle_post_request_for_trash_detail(request=request, trash=trash)
    context['content'] = trash.content.keys
    return render(request, 'trash_control/trash_detail.html', context)


def _handle_post_request_for_trash_detail(request, trash):
    if request.POST.get("clear_task_del"):
        delete_clear_task(trash)
    elif request.POST.get("recovery_task_del"):
        delete_recovery_task(trash)
    elif request.POST.get("remove_task_del"):
        delete_remove_task(trash)
    elif request.POST.get("clear_task_run"):
        clear_task_list = trash.cleartask_set.all()
        if len(clear_task_list) > 0:
            clear_task = clear_task_list[0]
            clear_task.run()
            clear_task.delete()
    elif request.POST.get("recovery_task_run"):
        recovery_task_list = trash.recoverytask_set.all()
        if len(recovery_task_list) > 0:
            recovery_task = recovery_task_list[0]
            recovery_task.run()
            recovery_task.delete()
    elif request.POST.get("remove_task_run"):
        remove_task_list = trash.removetask_set.all()
        if len(remove_task_list) > 0:
            remove_task = remove_task_list[0]
            remove_task.run()
            remove_task.delete()


def home_page(request):
    now = datetime.datetime.now()
    title = 'Home page. For trash view write correct url address!'
    return render(request, 'trash_control/home_page.html', {'current_date': now, 'title': title})
