# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-24 21:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trash_control', '0013_auto_20170624_1835'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='removedobject',
            name='trash',
        ),
        migrations.AlterField(
            model_name='removetask',
            name='reg_expression',
            field=models.CharField(blank=True, default='[.]+', help_text='Write regular expression for reg mode', max_length=100),
        ),
        migrations.DeleteModel(
            name='RemovedObject',
        ),
    ]
