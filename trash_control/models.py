# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from apps import TrashcontrolConfig
from smartRMV.rm.rm_functions import read_info_json, write_info_json
from smartRMV.rm.control import recovery_control, trash_cleaning_control, remove_control
from smartRMV.rm.cleaning_policy import get_cleaning_policy
from os import path
import re


class Trash(models.Model):
    name = models.CharField(max_length=30, help_text="Name for trash")
    path = models.CharField(max_length=150, default=TrashcontrolConfig.default_trash_path,
                            help_text="Path for new trash")
    size = models.IntegerField(default=0)
    files_count = models.IntegerField(default=0)
    last_clearing_date = models.DateField('date of creating')
    max_trash_size = models.IntegerField(blank=True, default=TrashcontrolConfig.max_trash_size)
    _content = dict()
    report = models.CharField(max_length=300, default='There is no report')

    def __unicode__(self):
        unicode_str = "name: {name} ----- path: {path} ----- size of trash: {size} ----- last clearing date:" \
                      " {date}".format(name=self.name, path=self.get_trash_path(), size=str(self.size),
                                       date=str(self.last_clearing_date))
        return unicode_str

    def get_trash_path(self):
        return path.join(self.path, self.name)

    @property
    def content(self):
        return read_info_json(self.get_content_file_path())

    @content.setter
    def content(self, cont_dict):
        _content = cont_dict
        write_info_json(self.get_content_file_path(), _content)

    def get_content_file_path(self):
        return path.join(self.get_trash_path() + 'INFO', 'content.json')

    def sub_size_and_filecount(self, files_size, files_count):
        self.size -= files_size
        self.files_count -= files_count

    def add_size_and_filecount(self, files_size, files_count):
        self.size += files_size
        self.files_count += files_count

    def path_in_trash(self, name):
        return path.join(self.get_trash_path(), name)

    def check_trash_and_file_in_trash(self, pth):
        if self.get_trash_path() == pth:
            return False
        if re.search(self.get_trash_path(), pth):
            return False
        return True

    class Meta:
        ordering = ["id"]


class RemoveTask(models.Model):
    # path = models.CharField(blank=True, max_length=100, help_text="Removed path")
    trash = models.ForeignKey(Trash)
    creating_date = models.DateTimeField('date of creating')
    force = models.BooleanField(default=False, help_text='Force mode')
    mode = models.CharField(max_length=6, choices=(('recurs', 'recursive delete'),
                            ('del', 'delete'), ('reg', 'delete by regex')), default='recurs', help_text="Select mode")
    reg_expression = models.CharField(blank=True, max_length=100, help_text="Write regular expression for reg mode",
                                      default='[a-z]+')

    def __unicode__(self):
        unicode_str = "Remove file or directory. Mode: {mode}".format(mode=str(self.mode))
        return unicode_str

    class Meta:
        ordering = ["creating_date"]

    def run(self):
        task_statistic_object = TaskStatisticObject.objects.create(type_task="Removing", content="", trash=self.trash,
                                                                   execution_date=timezone.datetime.now(), result="")
        task_statistic_object.content = "Paths: "
        self.trash.report = "Operation: {operation} ----- Result: operation was completed successfully!".format(
            operation=self)
        files_count = self.trash.files_count
        size = self.trash.size
        paths = self.pathsbyremovetask_set.all()
        for pth in paths:
            try:
                remove_control(self.mode, pth.path, self.trash, False, False,
                               self.trash.max_trash_size, self.reg_expression)
                task_statistic_object.content = "{content}{pth}, ".format(
                    content=task_statistic_object.content, pth=pth.path)
            except Exception as ex:
                if not self.force:
                    self.trash.report = "Operation: {operation} ----- \nException: {message}".format(
                        message=ex.message, operation=self)
                    break
        self.trash.save()
        task_statistic_object.result = "{statistic} was removed {count} files, size: {size} Kb".format(
            statistic=self.trash.report, count=self.trash.files_count - files_count,
            size=self.trash.size - size)
        task_statistic_object.save()


class PathsByRemoveTask(models.Model):
    path = models.CharField(max_length=100, help_text="Enter paths. Each path from a new line.")
    task = models.ForeignKey(RemoveTask)

    def __unicode__(self):
        unicode_str = "{path}".format(path=self.path)
        return unicode_str

    class Meta:
        ordering = ["task"]


class RecoveryTask(models.Model):
    trash = models.ForeignKey(Trash)
    creating_date = models.DateTimeField('date of creating')
    force = models.BooleanField(default=False, help_text='Force mode')

    def __unicode__(self):
        unicode_str = "Recovery files or directories "
        return unicode_str

    class Meta:
        ordering = ["creating_date"]

    def run(self):
        task_statistic_object = TaskStatisticObject.objects.create(
            type_task="Recovering", content="Recovered files and directories: ", trash=self.trash,
            execution_date=timezone.datetime.now(), result="")
        self.trash.report = "Operation: {operation} ----- Result: operation was completed successfully!".format(
            operation=self)
        files_count = self.trash.files_count
        size = self.trash.size
        removed_objects = self.trash.content
        names = self.namesforrecoverytask_set.all()
        for file_name in names:
            if unicode(file_name) in removed_objects.iterkeys():
                try:
                    recovery_control(file_name.name, self.trash, False, False)
                    task_statistic_object.content = "{content}{name}, ".format(
                        content=task_statistic_object.content, name=file_name.name)
                except Exception as ex:
                    if not self.force:
                        self.trash.report = "Operation: {operation} ------ \nException: {message}".format(
                            message=ex.message, operation=self)
                        break
            else:
                task_statistic_object.content = "{content}not found {name}, ".format(
                    content=task_statistic_object.content, name=file_name.name)
        self.trash.save()
        task_statistic_object.result = "{statistic} was recovered {count} files, size: {size}".format(
            statistic=task_statistic_object.result, count=abs(self.trash.files_count - files_count),
            size=abs(self.trash.size - size))
        task_statistic_object.save()


class NamesForRecoveryTask(models.Model):
    name = models.CharField(max_length=100, help_text="Enter names. Each name from a new line")
    task = models.ForeignKey(RecoveryTask)

    def __unicode__(self):
        unicode_str = "{name}".format(name=self.name)
        return unicode_str

    class Meta:
        ordering = ["task"]


class ClearTask(models.Model):
    policy_list = (("full_cleaning", "full cleaning"),
                   ("time_interval", "time interval"),
                   ("max_amount", "max amount"))
    policy = models.CharField(max_length=30, choices=policy_list, default="full_cleaning",
                              help_text='Select clear policy')
    trash = models.ForeignKey(Trash)
    creating_date = models.DateTimeField('date of creating')
    force = models.BooleanField(default=False, help_text='Force mode')
    max_amount = models.IntegerField(blank=True, default=TrashcontrolConfig.max_amount,
                                     help_text='Enter for max_amount policy')
    days = models.IntegerField(blank=True, default=TrashcontrolConfig.days,
                               help_text='Enter for time_interval policy')

    def __unicode__(self):
        unicode_str = "Clear trash by policy ----- policy: {policy}".format(policy=self.policy)
        return unicode_str

    class Meta:
        ordering = ["creating_date"]

    def clear_trash_statistic(self):
        if self.policy == "full_cleaning":
            self.trash.taskstatisticobject_set.all().delete()

    def run(self):
        task_statistic_object = TaskStatisticObject.objects.create(type_task="Clearing", content="", trash=self.trash,
                                                                   execution_date=timezone.datetime.now(), result="")
        task_statistic_object.content = "trash clearing --- {policy} --- force: {force}".format(
            policy=self.policy, force=self.force)
        self.clear_trash_statistic()
        files_count = self.trash.files_count
        size = self.trash.size
        policy = get_cleaning_policy(self.policy, self.trash.get_trash_path(), self.days, self.max_amount)
        try:
            trash_cleaning_control(self.trash, policy, False, False)
        except Exception as ex:
            self.trash.report = "Operation: {operation} \nException: {message}".format(
                message=ex.message, operation=self)
            task_statistic_object.result = "{message}".format(message=ex.message)
        else:
            message = "Operation was completed successfully!"
            self.trash.report = "Operation: {operation} \nResult: {message}".format(
                message=message, operation=self)
            task_statistic_object.result = "{message}".format(message=message)
        finally:
            self.trash.last_clearing_date = timezone.datetime.now().date()
            if self.policy == "full_cleaning":
                self.trash.files_count = 0
                self.trash.size = 0
            self.trash.save()
            task_statistic_object.result = "{statistic} was cleared {count} files, size: {size} Kb".format(
                statistic=task_statistic_object.result, count=abs(self.trash.files_count - files_count),
                size=abs(self.trash.size - size))
            task_statistic_object.save()


class TaskStatisticObject(models.Model):
    type_task = models.CharField(max_length=30)
    content = models.CharField(max_length=200)
    result = models.CharField(max_length=150)
    execution_date = models.DateField('Date of task execution')
    trash = models.ForeignKey(Trash)

    def __unicode__(self):
        unicode_str = "type of task: {type_task} ----- date of task execution: {date}".format(
            type_task=self.type_task, date=str(self.execution_date))
        return unicode_str

    class Meta:
        ordering = ["execution_date"]


"""class RemovedObject(models.Model):
    cur_name = models.CharField(max_length=100)
    old_path = models.CharField(max_length=150)
    removing_date = models.DateField('date of removing')
    trash = models.ForeignKey(Trash)

    def __unicode__(self):
        unicode_str = "name: {cur_name} ----- date of creating: {date}".format(
            cur_name=self.cur_name, date=str(self.removing_date))
        return unicode_str

    class Meta:
        ordering = ["removing_date"]"""