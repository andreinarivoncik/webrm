# -*- coding: utf-8 -*-
from django.http import HttpResponse
import datetime
from django.shortcuts import get_object_or_404, render
from django.template import Template, Context


def home_page(request):
    now = datetime.datetime.now()
    title = 'Error 404. Nothing found on your request. Write correct url adress!'
    return render(request, 'trash_control/home_page.html', {'current_date': now, 'title': title})
